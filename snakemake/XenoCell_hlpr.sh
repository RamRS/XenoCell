#!/bin/sh

#PBS -N XenoCell_pipeline
#PBS -V
#PBS -S /bin/sh
#PBS -l select=1:ncpus=1
#PBS -o <stdout_path>
#PBS -e <stderr_path>
#PBS -m ae
#PBS -M <your@email.it>

cd $PBS_O_WORKDIR

#snakemake v 4.0.0 or above

snakemake \
--snakefile XenoCell/snakemake/XenoCell_pipeline.snakefile \
-j 15 \
--latency-wait 90 \
--keep-going \
--directory  Data/analysis/ \
--configfile XenoCell/snakemake/config.yaml \
--cluster-config XenoCell/snakemake/pbs.json \
--cluster "qsub -V -S /bin/sh -N {cluster.name} -l ncpus={cluster.ncpus} -l mem={cluster.mem} -o {cluster.stdout} -e {cluster.stderr} -P {cluster.project_name}" \
--printshellcmds \
--allow-ambiguity
