"""
____________________________________________

Snakemake workflow to run XenoCell.
____________________________________________

"""
##----------------------------------------------------------------------------##
## Import python libraries and shell executable
##----------------------------------------------------------------------------##
import pandas as pd
shell.executable("bash")
import os

##----------------------------------------------------------------------------##
## Get list of samples.
##----------------------------------------------------------------------------##
samples = list(config['samples'].keys())
subsets = list(config['subsets'].keys())

##----------------------------------------------------------------------------##
## Create folder for cluster logs.
##----------------------------------------------------------------------------##
if not os.path.exists('.logs/'):
  os.makedirs('.logs/')

##----------------------------------------------------------------------------##
## Store --directory in variable workingdir
##----------------------------------------------------------------------------##
workingdir =  os.getcwd()

##----------------------------------------------------------------------------##
## Impose rule order indexing before classifying
##----------------------------------------------------------------------------##
ruleorder: index > classify_reads > extract_cellular_barcodes

##----------------------------------------------------------------------------##
## -- all
##----------------------------------------------------------------------------##
## Collect results for entire workflow.
##----------------------------------------------------------------------------##
rule all:
	input:
		fq_barcode = expand (workingdir +'/'+"{sample}/"+ "fq_barcode.fastq.gz", sample =samples),
		fq_transcript = expand (workingdir +'/'+"{sample}/"+ "fq_transcript.fastq.gz", sample =samples),
		table_host_vs_graft = expand(workingdir +'/'+"{sample}/"+"host_vs_graft_total.csv", sample =samples),
		plot = expand(workingdir +'/'+"{sample}/"+"host_vs_graft_total.png", sample =samples),
		fastq_barcode_gz_= expand(workingdir +'/'+"{sample}/"+"{subset}/"+"fq_barcode.fastq.gz", sample = samples, subset = subsets),
		fastq_transcript_gz = expand(workingdir +'/'+"{sample}/"+"{subset}/"+"fq_transcript.fastq.gz", sample = samples, subset = subsets),
		subset_plot = expand(workingdir +'/'+"{sample}/"+'host_vs_graft_total_subset_'+'{subset}'+'.png', sample = samples, subset = subsets),
		cellular_barcodes = expand(workingdir +'/'+"{sample}/"+"{subset}/"+"cellular_barcodes.txt", sample = samples, subset = subsets)


##----------------------------------------------------------------------------##
## -- gunzip_FASTQ
##----------------------------------------------------------------------------##
## Uncompress and Merge FastQ files
##----------------------------------------------------------------------------##
rule merge_gunzip_fastq:
	output:
		R1 = temp(workingdir +'/'+'{sample}/FASTQ/{sample}_R1.fastq'),
		R2 = temp(workingdir +'/'+'{sample}/FASTQ/{sample}_R2.fastq')
		#
		# R1_L001 = temp('{sample}/FASTQ/{sample}_L001_R1.fq'),
		# R1_L002 = temp('{sample}/FASTQ/{sample}_L002_R1.fq'),
		# R2_L001 = temp('{sample}/FASTQ/{sample}_L001_R2.fq'),
		# R2_L002 = temp('{sample}/FASTQ/{sample}_L002_R2.fq')
	params:
		PATH = lambda wildcards: config['samples'][wildcards.sample]['path']
	message:
	 	'--- Uncompress and Merge FASTQ files'
	shell:
		"""
		zcat {params.PATH}/*_R1*.fastq.gz > {output.R1} && \
		zcat {params.PATH}/*_R2*.fastq.gz > {output.R2}
		"""

##----------------------------------------------------------------------------##
## -- XenoCell_index
##----------------------------------------------------------------------------##
## Indexing Host and Graft reference genomes with Xenome
##----------------------------------------------------------------------------##
rule index:
	input:
		HOST_FASTA_FILE = config["filesdirs"]["host_fa"],
		GRAFT_FASTA_FILE = config["filesdirs"]["graft_fa"]
	output:
		a = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.lhs-bits',
		b = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.rhs-bits',
		c = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.kmers.high-bits',
		d = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.kmers-d0',
		e = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.kmers-d1',
		f = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.kmers.low-bits.upr',
		g = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.kmers.low-bits.lwr',
		h = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.kmers.header',
		i = config["filesdirs"]["xenome_index_outdir"]+'/idx-both.header',
		j = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.kmers.high-bits',
		k = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.kmers-d0',
		l = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.kmers-d1',
		m = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.kmers.low-bits.upr',
		n = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.kmers.low-bits.lwr',
		o = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.kmers.header',
		p = config["filesdirs"]["xenome_index_outdir"]+'/idx-host.header',
		q = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.kmers.high-bits',
		r = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.kmers-d0',
		s = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.kmers-d1',
		t = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.kmers.low-bits.upr',
		u = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.kmers.low-bits.lwr',
		v = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.kmers.header',
		y = config["filesdirs"]["xenome_index_outdir"]+'/idx-graft.header',
		Done = touch(config["filesdirs"]["xenome_index_outdir"]+"/indexing.done")
	params:
		OUTDIR = config["filesdirs"]["xenome_index_outdir"]+'/idx',
		MEM = config["parameters"]["mem_index"],
		nCPUS = config["parameters"]["nCPUS_index"]
	message:
		"--- Xenome: Index host and graft genomes"
	shell:
		"""
		/hpcnfs/software/singularity/3.5.3/bin/singularity exec \
		--bind /hpcnfs \
		/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.sif \
		xenocell.py generate_index \
		--host {input.HOST_FASTA_FILE} \
		--graft {input.GRAFT_FASTA_FILE} \
		--output {params.OUTDIR} \
		--threads {params.nCPUS} \
		--memory {params.MEM}
		"""

##----------------------------------------------------------------------------##
## -- XenoCell_classify_reads
##----------------------------------------------------------------------------##
## Classify reads with Xenome
##----------------------------------------------------------------------------##
rule classify_reads:
	input:
		index_done = rules.index.output.Done,
		transcript = rules.merge_gunzip_fastq.output.R2,
		barcode = rules.merge_gunzip_fastq.output.R1
	params:
		REFERENCE = config["filesdirs"]["xenome_index_outdir"],
		OUTDIR = workingdir +"/"+'{sample}/',
		MEM = config["parameters"]["mem_classify"],
		nCPUS = config["parameters"]["nCPUS_classify"],
		BC_START = config['barcode_infos']['barcode_start'],
		BC_LENGTH = config['barcode_infos']['barcode_length']
	output:
		fq_barcode = workingdir +'/'+"{sample}/"+"fq_barcode.fastq.gz",
		fq_transcript = workingdir +'/'+"{sample}/"+"fq_transcript.fastq.gz",
		table_host_vs_graft = workingdir +'/'+"{sample}/"+"host_vs_graft_total.csv",
		plot = workingdir +'/'+"{sample}/"+"host_vs_graft_total.png"
	message:
		"--- XenoCell: Classify reads"
	shell:
		"""
		/hpcnfs/software/singularity/3.5.3/bin/singularity exec \
		--bind /hpcnfs \
		/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.sif \
		xenocell.py classify_reads \
		--transcript {input.transcript} \
		--barcode {input.barcode} \
		--barcode_start {params.BC_START} \
		--barcode_length {params.BC_LENGTH} \
		--index {params.REFERENCE} \
		--output {params.OUTDIR} \
		--keep_xenome_output False \
		--threads {params.nCPUS} \
		--memory {params.MEM} \
		--compression_level 1
		"""

##----------------------------------------------------------------------------##
## -- XenoCell_extract_cellular_barcodes
##----------------------------------------------------------------------------##
## XenoCell gets rid of cellular barcodes having percentage of host reads outside of the thresholds range
##----------------------------------------------------------------------------##

rule extract_cellular_barcodes:
	input:
		fq_transcript = rules.classify_reads.output.fq_transcript,
		fq_barcode = rules.classify_reads.output.fq_barcode,
		table_host_vs_graft = rules.classify_reads.output.table_host_vs_graft,
		plot = rules.classify_reads.output.plot
	params:
		INPUTDIR = rules.classify_reads.params.OUTDIR,
		UP_THRESHOLD = lambda wildcards: config['subsets'][wildcards.subset]['upper_threshold'],
		LO_THRESHOLD = lambda wildcards: config['subsets'][wildcards.subset]['lower_threshold'],
		BC_START = config['barcode_infos']['barcode_start'],
		BC_LENGTH = config['barcode_infos']['barcode_length'],
		SUBSET = "{subset}",
		nCPUS = config["parameters"]["nCPUS_extract"]
	output:
		fastq_barcode_gz_ = protected(workingdir +'/'+"{sample}/"+"{subset}/"+"fq_barcode.fastq.gz"),
		fastq_transcript_gz = protected(workingdir +'/'+"{sample}/"+"{subset}/"+"fq_transcript.fastq.gz"),
		cellular_barcodes = (workingdir +'/'+"{sample}/"+"{subset}/"+"cellular_barcodes.txt"),
		subset_plot = (workingdir +'/'+"{sample}/"+'host_vs_graft_total_subset_'+'{subset}'+'.png')
	message:
		"--- XenoCell: Extract cellular barcodes"
	shell:
		"""
		/hpcnfs/software/singularity/3.5.3/bin/singularity exec \
		--bind /hpcnfs \
		/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.sif \
		xenocell.py extract_cellular_barcodes \
		--input {params.INPUTDIR} \
		--barcode_start {params.BC_START} \
		--barcode_length {params.BC_LENGTH} \
		--subset_name {params.SUBSET} \
		--lower_threshold {params.LO_THRESHOLD} \
		--upper_threshold {params.UP_THRESHOLD} \
		--threads {params.nCPUS} \
		--compression_level 1
		"""
