# To do's

The points below are suggestions for possible improvements in future XenoCell version.

- [ ] Create _in silico_ data set with different proportions of cells (from different species of origin).
- [ ] Suggest sensible threshold for graft/host-specific reads for extracting cellular barcodes.
- [ ] Implement checks and test.
  - [ ] Check whether expected input files are present.
  - [ ] Check whether expected output files were produced.
- [ ] Print warning when using multiple cores but `pigz` is not installed.
- [ ] __IDEA:__ Provide option to extract cellular barcodes with few reads that do not pass provided treshold? At present, XenoCell does not perform any correction of cellular barcodes, potentially resulting in the loss of few reads. In practice, we found this not to affect the final transcription profile, but it might be different in other circumstances. Instead of implementing error-correction, which might be computationally expensive, we could implement an option to also extract cellular barcodes that are either empty or likely the result of sequencing errors. In our QC plot that is produced in the read classification step, these barcodes have few reads (left side) __and__ have between 10-90% host-specific reads. Those cells should be safe to extract because they are too small to result in a host cell, but might contain reads that, after error-correction, end up in the graft cells that we want to extract. The important thing is to avoid extracting multiplets. So, we could ask the user whether he/she wants to extract also possibly degenerated cellular barcodes from the center, and if so, the user should provide (1) the lower threshold, e.g. 10%, (2) the upper threshold, e.g. 90%, and (3) the maximum number of reads that cellular barcode is allowed to have.
