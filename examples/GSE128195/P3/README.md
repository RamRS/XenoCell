# `GSE128195` / `P3` example data set

As an example, we applied XenoCell to a public data set downloaded from the GEO under accession number [`GSE128195`](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE128195).

## Download FASTQ

```sh
fasterq-dump --split-reads -p SRR8715441
```

## Run XenoCell

### Generate Xenome index

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.simg \
xenocell.py generate_index \
--host /hpcnfs/data/PGP/reference_genomes/UCSC/mm10/WholeGenomeFasta/mm10.fa \
--graft /hpcnfs/data/PGP/reference_genomes/UCSC/hg19/WholeGenomeFasta/hg19.fa \
--output /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10 \
--threads 16 \
--memory 60
##----------------------------------------------------------------------------##
## XenoCell: Generate Xenome index of reference genomes.
##----------------------------------------------------------------------------##
## FASTA of host reference genome:  /hpcnfs/data/PGP/reference_genomes/UCSC/mm10/WholeGenomeFasta/mm10.fa
## FASTA of graft reference genome: /hpcnfs/data/PGP/reference_genomes/UCSC/hg19/WholeGenomeFasta/hg19.fa
## Output directory:                /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10
## Number of threads:               12
## Memory in GB:                    60
##----------------------------------------------------------------------------##
# [2020-12-01 08:24:00] Generate index...
# [2020-12-01 10:17:51] Generation of Xenome index finished!
```

### Step A: Classify reads with XenoCell

We use the `classify_reads` function to classify reads and generate a table of cellular barcodes containing the percentage of graft- and host-specific reads for each of them.
In this case, the read that contains the transcript is `R2`, while `R1` contains the cellular barcode.

```sh
/hpcnfs/software/singularity/3.1.1/bin/singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.sif \
xenocell.py classify_reads \
--transcript /hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/fastq/SRR8715441_2.fq.gz \
--barcode /hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/fastq/SRR8715441_1.fq.gz \
--barcode_start 1 \
--barcode_length 12 \
--index /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10 \
--output /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/P3 \
--threads 16 \
--memory 60 \
--compression_level 1
##----------------------------------------------------------------------------##
## XenoCell: Classify reads and generate table of cellular barcodes.
##----------------------------------------------------------------------------##
## FASTQ file containing transcript:       /hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/fastq/SRR8715441_2.fq.gz
## FASTQ file containing cellular barcode: /hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/fastq/SRR8715441_1.fq.gz
## Barcode starting position:              1
## Length of cellular barcode:             12
## Path to Xenome index:                   /hpcnfs/data/PGP/reference_genomes/xenome/graft_hg19_host_mm10
## Output directory:                       /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/P3
## Keep Xenome output files:               False
## Number of threads:                      16
## Memory in GB:                           60
## Compression level:                      1
##----------------------------------------------------------------------------##
# [2020-12-01 23:31:56] De-compress FASTQ files before classifying...
# [2020-12-01 23:46:10] Start classification...
# [2020-12-02 00:25:45] Fix FASTQ format...
# [2020-12-02 00:35:47] Create list of read names to extract...
# [2020-12-02 00:41:51] Extract reads...
# [2020-12-02 00:56:41] Generate table of cellular barcodes and associated read counts...
# [2020-12-02 01:00:40] Number of unique cellular barcodes: 1,128,598
# [2020-12-02 01:00:40] Compress FASTQ files...
# [2020-12-02 01:12:16] Delete temporary files...
# [2020-12-02 01:12:20] Classification of reads finished!
```

This step creates a plot showing the fraction of graft/host-specific reads for each cellular barcode.

<p align="center"><img src="host_vs_graft_total.png" /></p>

Based on this plot, we can decide the thresholds when extracting cellular barcodes.

### Step B: Extract graft-specific cellular barcodes

To extract **graft**-specific cellular barcodes, we use the `extract_cellular_barcodes` function to extract reads from all cellular barcodes that contain between 0-10% of host-specific reads.

__Note__:
We could omit the `--lower_threshold` parameter since the default is `0.0`.

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.sif \
xenocell.py extract_cellular_barcodes \
--input /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/P3 \
--barcode_start 1 \
--barcode_length 12 \
--subset_name graft \
--lower_threshold 0.0 \
--upper_threshold 0.1 \
--threads 16 \
--compression_level 1
##----------------------------------------------------------------------------##
## XenoCell: Extract cellular barcodes.
##----------------------------------------------------------------------------##
## Input folder:               /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/P3
## Barcode starting position:  1
## Length of cellular barcode: 12
## Subset name:                graft
## Lower threshold:            0.0
## Upper threshold:            0.1
## Number of threads:          16
## Compression level:          1
##----------------------------------------------------------------------------##
# [2020-12-02 01:12:40] Load output data from read classification step...
# [2020-12-02 01:12:52] Generate list of cellular barcodes that pass the specified thresholds...
# [2020-12-02 01:12:53] Generate list of reads that belong to the cellular barcodes...
# [2020-12-02 01:39:35] Extract reads from original FASTQ files...
# [2020-12-02 02:16:52] Compress FASTQ files...
# [2020-12-02 02:26:34] Delete temporary files...
# [2020-12-02 02:26:37] Extraction of cellular barcodes finished!
#  - Number of unique cellular barcodes extracted: 879,386
#  - Number of reads extracted: 201,967,683
```

This step creates a plot highlighting the cellular barcodes which are extracted.
In this case, it looks as shown below.

<p align="center"><img src="host_vs_graft_total_subset_graft.png" /></p>

In the input folder (`/hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01_hgmm_5k_v3`) we now have an additional directory with the same name set as `subset_name`.
This folder contains the list of cellular barcodes associated with this subset, as well as the `R1` and `R2` FASTQ files which can then be used for further analysis.

### Step B: Extract host-specific cellular barcodes

Similar to before, to extract **host**-specific cellular barcodes, we use the `extract_cellular_barcodes` function to extract reads from all cellular barcodes that contain between 90-100% of host-specific reads.

__Note__:
We could omit the `--upper_threshold` parameter since the default is `1.0`.

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/scratch/PGP/xenocell/container/xenocell_1.0.simg \
xenocell.py extract_cellular_barcodes \
--input /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/hgmm_5k_v3 \
--barcode_start 1 \
--barcode_length 16 \
--subset_name host \
--lower_threshold 0.9 \
--upper_threshold 1.0 \
--threads 16 \
--compression_level 1
##----------------------------------------------------------------------------##
## XenoCell: Extract cellular barcodes.
##----------------------------------------------------------------------------##
## Input folder:               /hpcnfs/scratch/PGP/xenocell/test_runs/2020-12-01/P3
## Barcode starting position:  1
## Length of cellular barcode: 12
## Subset name:                host
## Lower threshold:            0.9
## Upper threshold:            1.0
## Number of threads:          16
## Compression level:          1
##----------------------------------------------------------------------------##
# [2020-12-02 02:26:45] Load output data from read classification step...
# [2020-12-02 02:26:53] Generate list of cellular barcodes that pass the specified thresholds...
# [2020-12-02 02:26:53] Generate list of reads that belong to the cellular barcodes...
# [2020-12-02 02:50:49] Extract reads from original FASTQ files...
# [2020-12-02 03:02:33] Compress FASTQ files...
# [2020-12-02 03:03:10] Delete temporary files...
# [2020-12-02 03:03:10] Extraction of cellular barcodes finished!
#  - Number of unique cellular barcodes extracted: 128,944
#  - Number of reads extracted: 11,655,658
```

<p align="center"><img src="host_vs_graft_total_subset_host.png" /></p>

## Align FASTQ

We use STARsolo `v2.7.5c` to align the FASTQ files generated by XenoCell.

### Unfiltered (no XenoCell)

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/data/PGP/singularity_imgs/3.5.3/star_2.7.5c.sif \
STAR \
--runThreadN 20 \
--genomeDir /hpcnfs/data/PGP/reference_genomes/UCSC/hg38/Indexes/STAR_2.7.5c_GENCODE_v27_90bp \
--readFilesIn \
/hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/fastq/SRR8715441_2.fastq \
/hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/fastq/SRR8715441_1.fastq \
--outFileNamePrefix ./ \
--outTmpKeep tmp/ \
--outStd Log \
--outSAMattributes NH HI AS nM CR CY UR UY \
--outSAMtype None \
--soloType CB_UMI_Simple \
--soloCBwhitelist None \
--soloCBstart 1 \
--soloCBlen 12 \
--soloUMIstart 13 \
--soloUMIlen 8 \
--soloBarcodeReadLength 20 \
--soloCBmatchWLtype 1MM_multi_pseudocounts \
--soloStrand Forward \
--soloFeatures Gene \
--soloUMIdedup 1MM_All \
--soloUMIfiltering MultiGeneUMI \
--soloOutFileNames ./ genes.tsv barcodes.tsv matrix.mtx \
--soloCellFilter CellRanger2.2 3000 0.99 10
```

### Graft cells on `hg38` reference genome

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/data/PGP/singularity_imgs/3.5.3/star_2.7.5c.sif \
STAR \
--runThreadN 20 \
--genomeDir /hpcnfs/data/PGP/reference_genomes/UCSC/hg38/Indexes/STAR_2.7.5c_GENCODE_v27_90bp \
--readFilesIn \
/hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/P3/graft/fq_transcript.fastq.gz \
/hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/P3/graft/fq_barcode.fastq.gz \
--readFilesCommand zcat \
--outFileNamePrefix ./ \
--outTmpKeep tmp/ \
--outStd Log \
--outSAMattributes NH HI AS nM CR CY UR UY \
--outSAMtype None \
--soloType CB_UMI_Simple \
--soloCBwhitelist None \
--soloCBstart 1 \
--soloCBlen 12 \
--soloUMIstart 13 \
--soloUMIlen 8 \
--soloBarcodeReadLength 20 \
--soloCBmatchWLtype 1MM_multi_pseudocounts \
--soloStrand Forward \
--soloFeatures Gene \
--soloUMIdedup 1MM_All \
--soloUMIfiltering MultiGeneUMI \
--soloOutFileNames ./ genes.tsv barcodes.tsv matrix.mtx \
--soloCellFilter CellRanger2.2 3000 0.99 10
```

### Host cells on `mm10` genome

```sh
singularity exec \
--bind /hpcnfs \
/hpcnfs/data/PGP/singularity_imgs/3.5.3/star_2.7.5c.sif \
STAR \
--runThreadN 20 \
--genomeDir /hpcnfs/data/PGP/reference_genomes/UCSC/mm10/Indexes/STAR_2.7.5c_GENCODE_vM16_90bp \
--readFilesIn \
/hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/P3/host/fq_transcript.fastq.gz \
/hpcnfs/scratch/PGP/xenocell/BMC_MG_review/PDX_public_dataset/P3/host/fq_barcode.fastq.gz \
--readFilesCommand zcat \
--outFileNamePrefix ./ \
--outTmpKeep tmp/ \
--outStd Log \
--outSAMattributes NH HI AS nM CR CY UR UY \
--outSAMtype None \
--soloType CB_UMI_Simple \
--soloCBwhitelist None \
--soloCBstart 1 \
--soloCBlen 12 \
--soloUMIstart 13 \
--soloUMIlen 8 \
--soloBarcodeReadLength 20 \
--soloCBmatchWLtype 1MM_multi_pseudocounts \
--soloStrand Forward \
--soloFeatures Gene \
--soloUMIdedup 1MM_All \
--soloUMIfiltering MultiGeneUMI \
--soloOutFileNames ./ genes.tsv barcodes.tsv matrix.mtx \
--soloCellFilter CellRanger2.2 3000 0.99 10
```