##----------------------------------------------------------------------------##
## Classify reads and generate table of cellular barcodes.
##----------------------------------------------------------------------------##
## User input:
##   - path to FASTQ file containing cell barcodes
##   - path to FASTQ file containing transcript
##   - barcode starting position
##   - length of cellular barcode
##   - path to Xenome index
##   - output directory
##   - number of threads
##   - memory in GB
##   - compression level
##----------------------------------------------------------------------------##
## Output:
##   - table of cellular barcodes and percentage of graft- and host-specific
##     reads + corresponding plot
##----------------------------------------------------------------------------##

#
import argparse
import collections
import csv
import glob
import gzip
import inspect
import matplotlib
import matplotlib.pyplot as plt
import ntpath
import os
import pandas as pd
import sys
import time

from common_functions import compress_file
from common_functions import copy_file
from common_functions import link_file
from common_functions import create_directory
from common_functions import decompress_file
from common_functions import print_log
from common_functions import read_file

#
matplotlib.use('Agg')

#
sys.dont_write_bytecode = True

#
def classify_reads(args):

  # print header with overview and parameter
  print_header(args)

  # Set barcode and FASTQ parameters right at the outset
  barcode_start_position = args.barcode_start - 1
  barcode_end_position = barcode_start_position + args.barcode_length
  fq_barcode = args.output + '/fq_barcode.fq.gz'
  fq_transcript = args.output + '/fq_transcript.fq.gz'
  
  ## Skip the following steps if skip_xenome is enabled
  if not args.skip_xenome:
    # create output directory and make it working directory
    create_directory(args.output)
    os.chdir(args.output)

    # prepare FASTQ files (link to them)
    link_file(args.barcode, fq_barcode)
    link_file(args.transcript, fq_transcript)    

    # check barcode-containing FASTQ file
    check_FASTQ_barcode_layout(fq_barcode, barcode_start_position, barcode_end_position)

    # classify reads with Xenome
    print_log('Starting classification...')
    run_xenome(fastq_in = fq_transcript, args = args)

    # Compress Xenome output
    print_log('Compressing Xenome output...')
    compress_fq_output(args = args)
  # fix FASTQ format
#  print_log('Fixing FASTQ format...')
#  fix_fastq_format(args.output + '/_host.fastq', args.output + '/fq_transcript_host.fq')
#  fix_fastq_format(args.output + '/_graft.fastq', args.output + '/fq_transcript_graft.fq')

  # extract read names
  print_log('Creating list of read names to extract...')
  extract_read_names(fastq_in = args.output + '/fq_transcript_host.fastq.gz', read_names_out = args.output + '/fq_transcript_host_reads.txt')
  extract_read_names(fastq_in = args.output + '/fq_transcript_graft.fastq.gz', read_names_out = args.output + '/fq_transcript_graft_reads.txt')

  # extract reads
  print_log('Extracting reads...')
  extract_corresponding_reads(read_names_in = args.output + '/fq_transcript_host_reads.txt',
                              fastq_in = fq_barcode, 
                              fastq_out = args.output + '/fq_barcode_host.fastq.gz')
  extract_corresponding_reads(read_names_in = args.output + '/fq_transcript_graft_reads.txt',
                              fastq_in = fq_barcode,
                              fastq_out = args.output + '/fq_barcode_graft.fastq.gz')

  # generate table of barcodes and read counts, save it to file, and make plot
  print_log('Generating table of cellular barcodes and associated read counts...')
  df_barcodes_all = generate_barcode_table(fq_graft = args.output + '/fq_barcode_graft.fastq.gz',
                                           fq_host = args.output + '/fq_barcode_host.fastq.gz',
                                           barcode_start_position = barcode_start_position,
                                           barcode_end_position = barcode_end_position)
  df_barcodes_all.to_csv(args.output + '/host_vs_graft_total.csv', index = False)
  generate_plot(table = df_barcodes_all,
                output_file = args.output + '/host_vs_graft_total.png')
  print_log('Number of unique cellular barcodes: ' + "{:,}".format(len(df_barcodes_all)))

  # compress FASTQ files if decompressed earlier
  #if args.transcript.lower().endswith('.gz'):
  #  print_log('Compressing input FASTQ files...')
  #  compress_file(args.output + '/fq_barcode.fq', args.compression_level, args.threads)
  #  compress_file(args.output + '/fq_transcript.fq', args.compression_level, args.threads)

  # remove intermediate files
  print_log('Deleting temporary files...')
  delete_temp_files(keep_xenome_files = args.keep_xenome_output, out_prefix = args.output)

  # Compress 
  # final message
  print_log('Classification of reads finished!')


#
def print_header(args):
  header = '''##----------------------------------------------------------------------------##
## XenoCell: Classify reads and generate table of cellular barcodes.
##----------------------------------------------------------------------------##
## FASTQ file containing transcript:       {transcript}
## FASTQ file containing cellular barcode: {barcode}
## Barcode starting position:              {barcode_start}
## Length of cellular barcode:             {barcode_length}
## Path to Xenome index:                   {index}
## Output directory:                       {output}
## Keep Xenome output files:               {keep_xenome_output}
## Number of threads:                      {threads}
## Memory in GB:                           {memory}
## Compression level:                      {compression_level}
## Skip Xenome:                            {skip_xenome}
##----------------------------------------------------------------------------##'''.format(
    transcript         = args.transcript,
    barcode            = args.barcode,
    barcode_start      = args.barcode_start,
    barcode_length     = args.barcode_length,
    index              = args.index,
    output             = args.output,
    keep_xenome_output = args.keep_xenome_output,
    threads            = args.threads,
    memory             = args.memory,
    compression_level  = args.compression_level,
    skip_xenome        = args.skip_xenome
  )
  print(header)


#
def check_FASTQ_barcode_layout(fastq_in, start_position, end_position):
  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({fastq_in}) does not exist.'.format(fastq_in=fastq_in))
  if start_position < 0:
    raise ValueError('Barcode start position must be 1 or larger.')
  # open barcode-containing FASTQ file and check length of barcode-containing
  # read; raise error if barcode end position is greater than length of
  # barcode-containing read
  with gzip.open(fastq_in, 'r') as fastq:
    fastq.readline()
    test_read = fastq.readline()
    test_read_length = len(str(test_read, 'UTF-8').rstrip("\n\r"))
    if end_position > test_read_length:
      raise ValueError('Barcode end position ({end_position}) seems '
        'to be larger than read length ({read_length}).'.format(
          end_position = end_position,
          test_read_length = test_read_length
        )
      )


#
def run_xenome(fastq_in, args):
  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({fastq_in}) does not exist.'.format(fastq_in=fastq_in))

  # Check if index is a directory. If so, add "/idx" to it. If not, use as such.
  if args.index[-1] == '/':
    index_with_prefix = args.index + 'idx'
  elif os.path.isdir(args.index):
    index_with_prefix = args.index + '/idx'
  elif os.path.exists(os.path.dirname(args.index)):
    index_with_prefix = args.index
  else:
    raise RuntimeError('Xenome index directory ({xen_index}) does not look right.'.format(xen_index=args.index))
  command = '''
    xenome classify \
    -i {fastq_in} \
    -P {index} \
    -T {threads} \
    -M {memory} \
    --output-filename-prefix {output}/fq_transcript > {output}/xenome_classify.stats
  '''.format(
    fastq_in = fastq_in,
    index    = index_with_prefix,
    threads  = args.threads,
    memory   = args.memory,
    output   = args.output
  )
  os.system(command)
  for file in ['fq_transcript_host.fastq', 'fq_transcript_graft.fastq']:
    if not os.path.isfile(args.output + '/' + file):
      raise RuntimeError('Expected output file ({file}) not found.'.format(file=file))

def compress_fq_output(args):
  '''
  Compress Xenome FQ output
  '''
  files_to_compress_glob = './fq_transcript_*.fastq'
  compress_file(files_to_compress_glob, args.compression_level, args.threads)
#
def fix_fastq_format(fastq_in, fastq_out):
  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({fastq_in}) does not exist.'.format(fastq_in=fastq_in))
  command = '''
    sed '1~4 s/^/@/g' {fastq_in} | sed '3~4 s/^/+/g' > {fastq_out}
  '''.format(
    fastq_in  = fastq_in,
    fastq_out = fastq_out
  )
  os.system(command)


#
def extract_read_names(fastq_in, read_names_out):
  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({fastq_in}) does not exist.'.format(fastq_in=fastq_in))
  command = '''
    zcat {fastq_in} | sed -n '1~4p' | sed 's/^.//' | sed 's/\s.*$//' > {read_names_out}
  '''.format(
    fastq_in       = fastq_in,
    read_names_out = read_names_out
  )
  os.system(command)
  if not os.path.isfile(read_names_out):
    raise RuntimeError('Expected output file ({file}) not found.'.format(file=read_names_out))


#
def extract_corresponding_reads(read_names_in, fastq_in, fastq_out):
  if not os.path.isfile(read_names_in):
    raise RuntimeError('Input read name file ({read_names_in}) does not exist.'.format(read_names_in=read_names_in))
  if not os.path.isfile(fastq_in):
    raise RuntimeError('Input FASTQ file ({fastq_in}) does not exist.'.format(fastq_in=fastq_in))
  command = '''
    mkdir $(basename {read_names_in} .txt).split
    split --suffix-length=4 --numeric-suffixes=1 --lines=100000000 {read_names_in} $(basename {read_names_in} .txt).split/read_names.part
    for f in $(basename {read_names_in} .txt).split/*
    do
      seqtk subseq {fastq_in} $f | gzip >> {fastq_out}
    done
    rm -rvf $(basename {read_names_in} .txt).split
  '''.format(
    read_names_in = read_names_in,
    fastq_in      = fastq_in,
    fastq_out     = fastq_out
  )
  os.system(command)
  if not os.path.isfile(fastq_out):
    raise RuntimeError('Expected output file ({file}) not found.'.format(file=fastq_out))


#
def generate_barcode_table(fq_graft, fq_host, barcode_start_position, barcode_end_position):
  barcodes_graft = read_file(fq_graft, barcode_start_position, barcode_end_position)
  barcodes_host = read_file(fq_host, barcode_start_position, barcode_end_position)
  barcodes_graft.columns = ['barcode', 'graft']
  barcodes_host.columns = ['barcode', 'host']
  barcodes_all = barcodes_host.merge(barcodes_graft, on = 'barcode', how = 'outer')
  barcodes_all = barcodes_all.fillna(value = 0)
  barcodes_all['total'] = barcodes_all.sum(axis = 1)
  barcodes_all['perc'] = barcodes_all['host'] / barcodes_all['total']
  barcodes_all = barcodes_all.sort_values(by = 'total', ascending = False)
  return(barcodes_all)


# generate plot of read counts per cellular barcode over percentage of reads
# originating from host
def generate_plot(table, output_file):
  plt.scatter(table['total'], table['perc'], s=1)
  plt.xlabel('Number of reads')
  plt.ylabel('Percentage of reads from host')
  plt.savefig(output_file, dpi=900)
  plt.close()
  if not os.path.isfile(output_file):
    raise RuntimeError('Expected output file ({file}) not found.'.format(file=output_file))


#
def delete_temp_files(keep_xenome_files, out_prefix = '.'):
  if keep_xenome_files == False:
    os.remove(out_prefix + '/fq_transcript_ambiguous.fastq.gz')
    os.remove(out_prefix + '/fq_transcript_both.fastq.gz')
    os.remove(out_prefix + '/fq_transcript_graft.fastq.gz')
    os.remove(out_prefix + '/fq_transcript_host.fastq.gz')
    os.remove(out_prefix + '/fq_transcript_neither.fastq.gz')
  os.remove(out_prefix + '/fq_transcript_host_reads.txt')
  os.remove(out_prefix + '/fq_transcript_graft_reads.txt')
  os.remove(out_prefix + '/fq_barcode_host.fastq.gz')
  os.remove(out_prefix + '/fq_barcode_graft.fastq.gz')
  
