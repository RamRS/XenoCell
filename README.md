# XenoCell

In single-cell experiment performed in the presence of two organisms, such as xenograft experiment, XenoCell allows to extract cellular barcodes that are specific to one of the two organisms.
XenoCell can be used to remove unwanted contaminating cells from the host organism, or to extract cells from both organisms to analyze them separately.

For further details, consider having a look at the official [XenoCell publication @ BMC Medical Genomics](https://bmcmedgenomics.biomedcentral.com/articles/10.1186/s12920-021-00872-8).

## Approach

In a nutshell, XenoCell is a Python-based wrapper around Xenome decorated with several functions for data processing.

XenoCell works by using an indexed reference genome consisting of two species to classify paired reads into graft- and host-specific.
Then, for every cellular barcode, the percentage of graft- and host-specific reads is calculated.
Based on this information, users can specify upper and lower tresholds, e.g. max. 10% of host-specific reads, and extract cellular barcodes that comply with this selection.
Using this procedure, it is possible to extract graft- and host-specific cellular barcodes (cells) and subsequently analyze them separately or simply remove contaminating cells.

## How to get and use XenoCell

We provide XenoCell as a [Docker image](https://hub.docker.com/r/romanhaa/xenocell).
It comes with all required tools pre-installed.
The container can also be converted to a Singularity container.

```bash
# Docker
docker pull romanhaa/xenocell:1.0

# Singularity
singularity build xenocell_1.0.sif docker://romanhaa/xenocell:1.0
```

In the following steps, we provide a brief overview of the general XenoCell procedure.
More detailed examples can be found in the `examples` section (see further down).

### Preparation: Create Xenome index

First, one has to build an index for Xenome based on the two reference genomes in the given experiment.
This can be done with the `generate_index` function.

```bash
singularity exec xenocell_1.0.sif xenocell.py generate_index --help
```

### Step A: Classify reads

Then, using the `classify_reads` function we use Xenome to calculate the percentage of graft- and host-specific reads for each cellular barcode.
This step might take some time depending on how many reads are present in the experiment.

```bash
singularity exec xenocell_1.0.sif xenocell.py classify_reads --help
```

As an output we will get a plot (see below) based on which we can decide thresholds for cellular barcodes to extract in the next step.

<p align="center"><img src="examples/hgmm_5k_v3/host_vs_graft_total.png" /></p>

### Step B: Extract cellular barcodes

Now we are ready to extract specific cellular barcodes with the `extract_cellular_barcodes` function.

```bash
singularity exec xenocell_1.0.sif xenocell.py extract_cellular_barcodes --help
```

If we were mainly interested in cells from the graft, we might tolerate a maximum of 10% of host-specific reads in a cell and specify this as `--lower_threshold 0.0` and `--upper_threshold 0.1`.
With those values, we would extract the cellular barcodes highlighted in the plot below.

<p align="center"><img src="examples/hgmm_5k_v3/host_vs_graft_total_subset_graft.png" /></p>

By adjusting the thresholds, it is also possible to extract cells from the host organism.

## Example data set

To highlight the potential of XenoCell, we applied it to the following example data sets:

* [`hgmm_5k_v3`](examples/hgmm_5k_v3/)

## Citation

If you used XenoCell for your research, please cite the following publication:

Cheloni, S., Hillje, R., Luzi, L. et al. XenoCell: classification of cellular barcodes in single cell experiments from xenograft samples. BMC Med Genomics 14, 34 (2021). https://doi.org/10.1186/s12920-021-00872-8

## Acknowledgements

* Conway *et al.* (2012) Xenome — a tool for classifying reads from xenograft samples. *Bioinformatics*. [Article](https://doi.org/10.1093/bioinformatics/bts236) | [GitHub](https://github.com/data61/gossamer)

## Credits

Project avatar icon made by [Monkik](https://www.flaticon.com/authors/monkik/) from [flaticon.com](https://www.flaticon.com/) is licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/)

## License

XenoCell is licensed under the [MIT License (MIT)](LICENSE.md)
